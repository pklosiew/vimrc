" Przemyslaw Klosiewicz, 2013
" 
" Many things are inspired by: http://nvie.com/posts/how-i-boosted-my-vim/
"
" Packages:
"
" * clang_complete: https://github.com/Rip-Rip/clang_complete.git
" * ctrlp: https://github.com/kien/ctrlp.vim.git
" * nerdcommenter: https://github.com/scrooloose/nerdcommenter
" * nerdtree: https://github.com/scrooloose/nerdtree
" * vim-markdown: https://github.com/hallison/vim-markdown.git
"
" Colorschemes:
"
" * vim-colors-solarized: https://github.com/altercation/vim-colors-solarized.git
" * mustang: https://github.com/croaker/mustang-vim
" * wombat256: https://github.com/vim-scripts/wombat256.vim
"
" Use pathogen for vim bundle management
" see: http://tammersaleh.com/posts/the-modern-vim-config-with-pathogen
call pathogen#infect()
call pathogen#helptags()

" Non vi-compatible mode
set nocompatible

" Enable file type recognition
filetype plugin indent on

" Syntax coloring
syntax on

" Font settings
"set guifont=DejaVuSansMonoForPowerline:h9 " Try to decide if this looks good
set guifont=Menlo-RegularForPowerline:h9 " This is the default font (default size is 11)
"set guifont=Menlo:h9 " This is the default font (default size is 11)
"set guifont=DroidSansMono:h9 " This is the default font (default size is 11)
set antialias " noantialias if things go slow
" Alternative fonts to try
"set guifont=Pragmata_TT:h10
"set guifont=Inconsolata:h11
"set guifont=ProggyTinyTT:h15


set nu " Show line numbers
set nowrap " No line wrapping
set hidden " Cfr: http://nvie.com/posts/how-i-boosted-my-vim/
set backspace=indent,eol,start " http://vim.wikia.com/wiki/Backspace_and_delete_problems

" Show current mode at bottom of the screen
set showmode

" Options for pattern search: incremental, highlight, case insensitive (unless
" uppercase is used, i.e.: smart)
set incsearch
set hlsearch
set ignorecase
set smartcase

" Indentation (spaces instead of tabs)
set tabstop=4       " One tab is 4 spaces long
set shiftwidth=4    " One indentation is 4 spaces long
"set smarttab        " See manpage (not sure if useful)

" Comment out when working on VLeaf:
"set expandtab       " Use spaces instead of tabs

" Hide GUI toolbar
set go-=T

" Don't create backup/swap files (avoid polluting current dir)
set nobackup
set noswapfile

" Select a color scheme
"colorscheme summerfruit
"colorscheme mustang
colorscheme wombat256
"set background=dark
"colorscheme solarized

" > Vim 7.3: Highlight line at column 80
set colorcolumn=80
highlight ColorColumn ctermbg=lightgrey guibg=black

" Highlight current line
set cursorline

" Auto-reload .vimrc after update
au! BufWritePost $MYVIMRC source $MYVIMRC

" Map F7 and F9 to prev and next buffers
" (Convenient on a MacBook: F7 and F9 are the backwards/forwards buttons for
" controlling iTunes)
map <F7> :bp<CR>
map <F9> :bn<CR>

" Set folding method to use markers
set foldmethod=marker

" Use syntax-based folding for XML
let g:xml_syntax_folding=1
au FileType xml setlocal foldmethod=syntax

" Let Ctrl-p ignore the directory 'target' which I commonly use for CMake
" generated build files
let g:ctrlp_custom_ignore = 'target\/'

" Search for ctags files in cwd of current file or upwards
"set tags=./tags,tags;/
" Additional ctags
"set tags+=/Users/pklosiew/Documents/ctags/stl.tags
"set tags+=/Users/pklosiew/Documents/ctags/hdf5.tags

" OmniCppComplete
"let OmniCpp_MayCompleteDot = 1 " autocomplete with .
"let OmniCpp_MayCompleteArrow = 1 " autocomplete with ->
"let OmniCpp_MayCompleteScope = 1 " autocomplete with ::
"let OmniCpp_SelectFirstItem = 2 " select first item (but don't insert)
"let OmniCpp_NamespaceSearch = 2 " search namespaces in this and included files
"let OmniCpp_ShowPrototypeInAbbr = 1 " show function prototype (i.e. parameters) in popup window
"let OmniCpp_DefaultNamespaces = ["std"]

" Regenerate ctags with ctrl-F12
"map <C-F12> :!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<CR>

" Options for clang_complete, use clang binary for now
"let g:clang_exec="/opt/local/bin/clang-mp-3.1"
" Alternatively, the clang lib can be used, which should be faster. Doesn't
" work well on a mac though
"let g:clang_library_path="/opt/local/libexec/llvm-3.1/lib"
" Open a window with error message if something goes wrong
"let g:clang_complete_copen=1
let g:clang_use_library=1

" Powerline: https://github.com/Lokaltog/powerline
set rtp+=~/.vim/bundle/powerline/powerline/bindings/vim
set laststatus=2

